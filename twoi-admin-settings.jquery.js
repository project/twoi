/*
 *
 *  Author: Michal Rehak 
 * 
 */  


if(Drupal.jsEnabled) {
  $(document).ready(function() {
    if ($("input[@name='twoi_codeType']:checked").val() != 'custom') {
      $('#edit-twoi-cfgCustomCode-wrapper').hide();
    }
    
    $("input[@name='twoi_codeType']").change(function() {
      if ($("input[@name='twoi_codeType']:checked").val() == 'custom') {
        $('#edit-twoi-cfgCustomCode-wrapper').show();
      } else {
        $('#edit-twoi-cfgCustomCode-wrapper').hide();
      }
    });

    return false;
  });
}
