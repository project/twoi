/*
 *
 *  Author: Michal Rehak 
 * 
 */  

/*
 *  overrided function - hide upload button and show uploaded picture widget
 */ 
upload2i.prototype.handleResultCode = function ( codetxt )
	{
		this.target.value = codetxt;

		$('#button_twoi').hide();
		$('#imgthumb_twoi #thumb_twoi').html(codetxt);
		$('#imgthumb_twoi').show();
	};


if(Drupal.jsEnabled) {
  $(document).ready(function() {
    
    /*$('.imgthumb_2i').hide();*/
    
    // remove picture clicked - 
    $('#remove_twoi a').click( function() {

      $('#api_twoi textarea').val('');
      $('#imgthumb_twoi').hide();
      $('#button_twoi').show();
      return false;
    });

    return false;
  });
}
